<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePedidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pedidos', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('fecha_entrega');
            $table->enum('hora_entrega', array('1h - 1h:15m', '1h - 1h:30m', '1h - 1h:45m', '1h - 2h', '2h - 2h:15m', '2h - 2h:30m', '2h - 2h:45m', '2h - 3h', '3h - 3h:15m', '3h - 3h:30m', '3h - 3h:45m', '3h - 4h', '4h - 4h:15m', '4h - 4h:30m', '4h - 4h:45m', '4h - 5h', '5h - 5h:15m', '5h - 5h:30m', '5h - 5h:45m', '5h - 6h', '6h - 6h:15m', '6h - 6h:30m', '6h - 6h:45m', '6h - 7h', '7h - 7h:15m', '7h - 7h:30m', '7h - 7h:45m', '7h - 8h'));
            $table->unsignedInteger('driver_id');
            $table->foreign('driver_id')->references('id')->on('drivers');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pedidos');
    }
}
