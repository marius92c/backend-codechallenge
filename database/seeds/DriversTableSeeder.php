<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
class DriversTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        for($i = 0; $i < 20; $i++){
            DB::table('drivers')->insert([
                'nombre' => $faker->name(),
                'apellidos' => $faker->name(),
                'telefono' => mt_rand(100000000,999999999),
                'email' => $faker->email,
                'direccion_entrega' => $faker->streetAddress,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
        }
    }
}
