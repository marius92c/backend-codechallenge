<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Declaración de las llamadas API REST.

Route::post('drivers', 'DriverController@store');
Route::post('pedidos', 'PedidoController@store');
Route::get('pedidos/{id}', 'PedidoController@show');