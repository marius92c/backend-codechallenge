<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pedido extends Model
{

    protected $fillable = ['fecha_entrega', 'hora_entrega','driver_id'];

    public function driver()
    {
        return $this->belongsTo('App\Driver');
    }
}
