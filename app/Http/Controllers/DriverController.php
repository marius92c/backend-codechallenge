<?php

namespace App\Http\Controllers;

use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Carbon\Carbon;
use App\Driver;

class DriverController extends Controller
{

    public function store(Request $request){

        // Recupero los parámetros pasados a través de la llamada
        $data = $request->all();

        try {

            // Creo un pedido con la información correspondiente
            return Driver::create([
                'nombre' => !empty($data['nombre']) ? $data['nombre'] : null,
                'apellidos' => !empty($data['apellidos']) ? $data['apellidos'] : null,
                'telefono' => !empty($data['telefono']) ? $data['telefono'] : null,
                'email' => !empty($data['email']) ? $data['email'] : null,
                'direccion_entrega' => !empty($data['direccion_entrega']) ? $data['direccion_entrega'] : null,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);

        }catch(QueryException $e){
            $errorCode = $e->errorInfo[1];

            // Compruebo si efectivamente hay registros duplicados, ya sea por el campo "email" o el campo "direccion_entrega", ya que son campos únicos
            // que no se pueden almacenar más de una vez
            if($errorCode == 1062){
                $errorResponse = array(
                    'error' => 'Registro duplicado'
                );
               return Response::create($errorResponse);
            }
        }

    }

}
