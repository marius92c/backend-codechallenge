<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use Carbon\Carbon;
use App\Pedido;
use App\Driver;

class PedidoController extends Controller
{

    /**
     * Método que muestra los pedidos en base al id del driver
     * @param $id
     * @return array
     */
    public function show($id)
    {

        // Obtengo el listado de pedidos de la base de datos
       $pedidos = Driver::with('pedidos')->find($id);

       // Declaro la variable para almacenar la información adicional al listado de pedidos
       $listadoPedidos = array();

       // Compruebo si efectivamente hay pedidos almacenados
       if(!empty($pedidos["pedidos"])){

            $arrPedidos = $pedidos["pedidos"];

            // Recorro el listado de pedidos para poder almacenar la información adicional por cada pedido
            foreach ($arrPedidos as $pedido){

                $listadoPedidos[] = array(
                    "Nombre" => $pedidos->nombre,
                    "Apellidos" => $pedidos->apellidos,
                    "Email" => $pedidos->email,
                    "Teléfono" => $pedidos->telefono,
                    "Dirección de entrega" => $pedidos->direccion_entrega,
                    "Fecha de entrega" => $pedido->fecha_entrega,
                    "Hora de entrega" => $pedido->hora_entrega,
                );

            }
       }

       // Devuelvo la respuesta con el listado de pedidos y su información relacionada
       return $listadoPedidos;
    }

    /**
     * Método que almacena los pedidos en base a la información pasada por el cuerpo del payload
     * @param Request $request
     * @return \Exception|QueryException
     */
    public function store(Request $request)
    {
        // Recupero los parámetros pasados a través de la llamada
        $data = $request->all();

        try {

            // Recupero un driver de manera aleatoria
            $driver = Driver::inRandomOrder()->first();

            // Creo un pedido con la información correspondiente
            return Pedido::create([
                'fecha_entrega' => !empty($data['fecha_entrega']) ? Carbon::createFromFormat('d/m/Y',$data['fecha_entrega'])->format('Y-m-d H:i:s') : null,
                'hora_entrega' => !empty($data['hora_entrega']) ? $data['hora_entrega'] : null,
                'driver_id' => $driver->id,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);

        }catch(QueryException $e){
           return $e;
        }
    }
}
