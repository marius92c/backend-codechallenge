<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Driver extends Model
{
    protected $fillable = ['nombre', 'apellidos','email', 'telefono', 'direccion_entrega'];

    public function pedidos()
    {
        return $this->hasMany('App\Pedido');
    }
}
